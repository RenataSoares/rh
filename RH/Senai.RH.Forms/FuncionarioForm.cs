﻿using RH.Senai.RH._Model;
using RH.Senai.RH.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class FuncionarioForm : Form
    {
        public FuncionarioForm()
        {
            InitializeComponent();
        }



        private void btnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNomeFuncionario.Text) && string.IsNullOrEmpty(txtCpfFuncionario.Text) && string.IsNullOrEmpty(txtEmailFuncionario.Text)
                && string.IsNullOrEmpty(txtRgFuncionario.Text) && string.IsNullOrEmpty(txtTelefoneFuncionario.Text))

            {
                MessageBox.Show("Preencha todos os campos !!");
            }
            else
            {


                // instancia um funcionário
                Funcionario funcionario = new Funcionario();

                //obtem o id do funcionario
                if (!string.IsNullOrEmpty(txtIDFuncionario.Text))
                {
                    //cria um id 
                    long id = 0;

                    //converte o texto do text box para long
                    if (long.TryParse(txtIDFuncionario.Text, out id))
                    {
                        //atribui o id 
                        funcionario.ID = id;

                    }
                }
                // atribui dados ao funcionário
                funcionario.Nome = txtNomeFuncionario.Text;
                funcionario.CPF = txtCpfFuncionario.Text;
                funcionario.RG = txtRgFuncionario.Text;
                funcionario.Email = txtEmailFuncionario.Text;
                funcionario.Telefone = txtTelefoneFuncionario.Text;

                // instancia o dao de funcionários
                FuncionarioDao dao = new FuncionarioDao();

                // salva o funcionário no banco de dados
                dao.Salvar(funcionario);

                PreencheDados();

                LimparFormulario();

            }
        }// fim do evento de click

            // métodos do programador
            private void LimparFormulario()
            {
                txtNomeFuncionario.Clear();
                txtCpfFuncionario.Clear();
                txtRgFuncionario.Clear();
                txtEmailFuncionario.Clear();
                txtTelefoneFuncionario.Clear();
                txtCpfFuncionario.Focus();
            }

            private void FuncionarioForm_Load(object sender, EventArgs e)
            {
                PreencheDados();
            }

            private void PreencheDados()
            {

                //instancia uma dao 
                FuncionarioDao dao = new FuncionarioDao();

                //preenche o datagrid view 
                dgvFuncionarios.DataSource = dao.Consultar();

                //oculta algumas colunas 
                dgvFuncionarios.Columns["ID"].Visible = false;
                dgvFuncionarios.Columns["Rg"].Visible = false;

                //limpa a selecao do data grid 
                dgvFuncionarios.ClearSelection(); //faz com que nao tenha um item selecionado quando abre o formulario 

                //limpa os campos do formulario 
                LimparFormulario();

            }
            

        private void dgvFuncionarios_SelectionChanged(object sender, EventArgs e)
        {
            // se alguma linha for selecionada
            if(dgvFuncionarios.CurrentRow != null)
            {
                // pega o id e coloca no textbox de id
                txtIDFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[0].Value.ToString();
                txtNomeFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[1].Value.ToString();
                txtCpfFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[2].Value.ToString();
                txtRgFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[3].Value.ToString();
                txtEmailFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[4].Value.ToString();
                txtTelefoneFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[5].Value.ToString();

            }
        }

        private void btnExcluirFuncionario_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIDFuncionario.Text))
            {
                string msg = "Selecione um funcionario na lista abaixo!";
                string titulo = "Operação nao realizada....";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK,MessageBoxIcon.Warning);

            }
            else
            {
                Funcionario funcionario = new Funcionario();

                long id = 0;

                if (long.TryParse(txtIDFuncionario.Text, out id))
                {
                    funcionario.ID = id; 

                }
                FuncionarioDao dao = new FuncionarioDao();


                //a variavel dialog recebe a resposta do metodo show 
                DialogResult resposta = MessageBox.Show("Deseja realmente excluir esse funcionario?", "Mensagem....", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //metodo equals compara se é igual 
                if (resposta.Equals(DialogResult.Yes))
                {
                    //atualiza o data grid 
                    PreencheDados();
                }


                //exclui o funcionario 
                dao.Excluir(funcionario);

                //atualiza o data grid 
                PreencheDados();

            }
        }

        private void txtCpfFuncionario_Leave(object sender, EventArgs e)
        {
            //guarda o texto do textbox de cpf 
            string cpf = txtCpfFuncionario.Text;

            Funcionario funcionario = new FuncionarioDao().Consultar(cpf);

            if (funcionario != null)
            {
                txtIDFuncionario.Text = funcionario.ID.ToString();
                txtCpfFuncionario.Text = funcionario.CPF;
                txtRgFuncionario.Text = funcionario.RG;
                txtNomeFuncionario.Text = funcionario.Nome;
                txtEmailFuncionario.Text = funcionario.Email;
                txtTelefoneFuncionario.Text = funcionario.Telefone;
            }
        }
    } // fim da classe
}
