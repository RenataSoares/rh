﻿using RH.Senai.RH.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class DependenteForm : Form
    {
        public DependenteForm()
        {
            InitializeComponent();
        }

        private void DependenteForm_Load(object sender, EventArgs e)
        {
            //preenche o datagrid
            PreencheDados();
        }
        private void PreencheDados()
        {
            dgvDependentes.DataSource = new DependenteDao().Consultar();
        }
    }
}
