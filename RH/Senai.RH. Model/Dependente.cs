﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RH.Senai.RH._Model
{
    class Dependente
    {
        private long id;

        public long ID
        {
            get { return id; }
            set { id = value; }
        }

    
        private string nome;

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        private string cpf;

        public string CPF
        {
            get { return cpf; }
            set { cpf = value; }
        }

        private DateTime dataNascimento;

        public DateTime DataNascimento
        {
            get { return dataNascimento; }
            set { dataNascimento = value; }
        }

        //composicao - relacao tem um 
        private Dependencia dependencia;

        public Dependencia Dependencia
        {
            get { return dependencia; }
            set { dependencia = value; }
        }

        private Funcionario funcionario;

        public Funcionario Funcionario
        {
            get { return funcionario; }
            set { funcionario = value; }
        }

        //construtor 

        public Dependente()
        {

        }

        public Dependente(long id, string nome, string cpf, DateTime dataNascimento, Dependencia dependencia, Funcionario funcionario)
        {
            this.id = id;
            this.nome = nome;
            this.cpf = cpf;
            this.dataNascimento = dataNascimento;
            this.dependencia = dependencia;
            this.funcionario = funcionario;
        }
    }
}
