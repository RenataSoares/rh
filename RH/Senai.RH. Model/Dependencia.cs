﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RH.Senai.RH._Model
{
    class Dependencia
    {
        private long id;

        public long ID
        {
            get { return id; }
            set { id = value; }
        }
        private string descricao;

        public string   Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        internal void Add(Dependencia dependencia)
        {
            throw new NotImplementedException();
        }
        public Dependencia ()
        {

        }
        public Dependencia(long id, string descricao)
        {
            this.id = id;
            this.descricao = descricao;
        }

        public override string ToString()
        {
            return this.descricao;
        }


    }

}
