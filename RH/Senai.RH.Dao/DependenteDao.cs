﻿using RH.Senai.RH._Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class DependenteDao : IDao<Dependente>
    {

        //variaveis do dao 
        //conexao com o banco 

        private SqlConnection connection;

        //comando do banco 
        private string sql = null;

        string msg = null;
        string titulo = null; 

        //construtor 
        public DependenteDao()
        {
            //conecta no banco 
            connection = new ConnectionFactory().GetConnection();
        }
        public List<Dependente> Consultar()
        {
            //instrucao sql 

            sql = "SELECT * FROM DependenteView ";

            //lista de dependentes 
            List<Dependente> dependentes = new List<Dependente>();

            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(sql, connection); //comunica com o banco 
                SqlDataReader leitor = cmd.ExecuteReader(); // cria o leitor para receber as coisas do banco 
                while (leitor.Read())
                {

                    //cria a dependentencia 
                    Dependencia dependencia = new Dependencia();
                    dependencia.ID = (long)leitor["IDDependencia"];
                    dependencia.Descricao = leitor["Descricao"].ToString();

                    //cria o funcionario 
                    Funcionario funcionario = new Funcionario();
                    funcionario.ID = (long)leitor["IDFuncionario"];
                    funcionario.Nome = leitor["NomeFuncionario"].ToString();
                    funcionario.CPF = leitor["CPFFuncionario"].ToString();
                    funcionario.RG = leitor["RG"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();



                    Dependente dependente = new Dependente();

                    dependente.ID = (long)leitor["IDDependente"];
                    dependente.Nome = leitor["NomeDependente"].ToString();
                    dependente.CPF = leitor["CPFDependente"].ToString();
                    dependente.DataNascimento = (DateTime)leitor["DataNascimento"];
                    dependente.Dependencia = dependencia;
                    dependente.Funcionario = funcionario;

                    dependentes.Add(dependente);

                }

            }
            catch (SqlException ex)
            {
                msg = "Erro ao consultar os dependentes\nErro: " + ex.Message;
                titulo = "Erro ....";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return dependentes; 
        }

        public Dependente Consultar(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Excluir(Dependente t)
        {
            throw new NotImplementedException();
        }

        public void Salvar(Dependente t)
        {
            throw new NotImplementedException();
        }
    }
}
