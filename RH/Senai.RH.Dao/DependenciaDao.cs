﻿using RH.Senai.RH._Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class DependenciaDao : ID<Dependencia>
    {

        private SqlConnection connection;
        private string sql = null;
        private string msg = null;
        private string titulo = null;

        public DependenciaDao()
        {
            connection = new ConnectionFactory().GetConnection();
        }

        public List<Dependencia> Consultar()
        {
            sql = "SELECT * FROM Dependentes";
            List<Dependencia> dependencias = new List<Dependencia>();
            try
            {
                // abre a conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // cria um leitor de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                // enquanto o leitor tiver dados para ler
                while (leitor.Read())
                {
                    Dependencia dependencia = new Dependencia();
                    dependencia.ID = (long)leitor["IDFuncionario"];
                    dependencia.Descricao = leitor["Dependencia"].ToString();

                    dependencia.Add(dependencia);

                }
            }
            catch (SqlException ex)
            {
                msg = "Erro ao consultar os dependentes cadastrados: " + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return dependencias;
        }

        public Dependencia Consultar(string parametro)
        {
            throw new NotImplementedException();
        }


        public void Excluir(Dependencia dependencia)
        {
        }

        public void Salvar(Dependencia dependencia)
        {
            //verifica se o id do funcionario é diferente de 0 
            if (dependencia.ID != 0)
            {
                //update 
                sql = "UPDATE Dependenciaa SET ID=@ID, Descricao=@Descricao WHERE IDDependencia = @IDDependencia";
            }
            else
            {
                //insert
                sql = "INSERT INTO Dependencia(ID,Descricao) VALUES (@ID, @Descricao)";
            }
            try
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@IDDependencia", dependencia.Descricao);
                cmd.Parameters.AddWithValue("@IDDescricao", dependencia.ID);

                cmd.ExecuteNonQuery();

                msg = "Dependencia " + dependencia.Descricao + " salvo com sucesso !";
                titulo = "Sucesso...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (SqlException ex)
            {
                msg = "Erro o salvar o dependencia: " + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();

            }
        }
    }
}